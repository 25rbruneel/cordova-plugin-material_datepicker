#!/usr/bin/env node

module.exports = function(ctx) {
    console.log('ready to execute copy of theme...');

    var path = require('path');
    var fs = require('fs');

    var platformRoot = path.join(ctx.opts.projectRoot, 'platforms/android');
    var themeRoot = path.join(ctx.opts.plugin.pluginInfo.dir, 'www/android/res');

    var srcFile = path.join(themeRoot, 'styles.xml');
    var destFile = path.join(platformRoot, 'res/values/styles.xml');

    console.log('Copying files... ');

    fs.createReadStream(srcFile).pipe(fs.createWriteStream(destFile));
}